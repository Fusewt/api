var express = require('express') // เรียกใช้งาน express mudule
var router = express.Router() // กําหนด router instance ให้กับ express.Routerclass
// เราใช้คําสั่ง use() เพื่อเรียกใช้งาน middleware function
// middleware ที่กํางานใน router instance ก่อนเข้าไปทํางานใน route function
router.use(function timeLog (req, res, next){ 
    console.log('Time: ', Date.now())
    next()
})
// กําหนด route หลัก หรือ root route
router.get('/', function (req, res) {
    res.send('Birds home page')
})
// กําหนด route เพิ่มเติม
router.get('/about', function (req, res) {
    res.send('About birds')
})

module.exports = router // ส่ง router ที่เราสร้าง ออกไปใช้งานภายนอกไฟล์