const memberRoute = require("express").Router();
memberRoute.get("/", (req, res) => {
  res.json({
    success: true,
  });
});
memberRoute.post("/register", (req, res) => {
  const { member_name, member_username, member_password } = req.body;
  res.json({
    success: true,
    member_name,
    member_username,
    member_password,
  });
});
module.exports = memberRoute;
