const productRoute = require("express").Router();
productRoute.get("/", (req, res) => {
  res.json({
    success: true,
  });
});
productRoute.post("/newproduct", (req, res) => {
  const { product_size, product_colors, product_band } = req.body;
  res.json({
    success: true,
    product_size,
    product_colors,
    product_band,
  });
});
module.exports = productRoute;
