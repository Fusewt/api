const express = require("express"); // ใช้งาน module express
const app = express(); // สร้างตัวแปร app เป็น instance ของ express
const config = require("config"); // เรียกใช้งานและกําหนด Instance ของโมดูล config
const PORT = config.get("port");
const authenRoute = require("./route/Authen");
const memberRoute = require("./route/Member");
const productRoute = require("./route/product");
app.use(express.json());
app.use("/authen", authenRoute);
app.use("/member", memberRoute);
app.use("/product", productRoute);
app.get("/", (req, res) => {
  res.json({
    success: true,
  });
});
app.listen(PORT, function () {
  console.log(`Server is runnung on port ${PORT}!`);
});
